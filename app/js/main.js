$('.slider-territory').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.territory-btn--prev'),
    nextArrow: $('.territory-btn--next'),
})
$('.slider-transport').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.transport-btn--prev'),
    nextArrow: $('.transport-btn--next'),
})
$('.slider-tokyo').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.tokyo-bnt--prev'),
    nextArrow: $('.tokyo-btn--next'),
})
$('.slider-bergen').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.bergen-btn--prev'),
    nextArrow: $('.bergen-btn--next'),
})
$('.slider-newyork').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.newyork-btn--prev'),
    nextArrow: $('.newyork-btn--next'),
})
$('.slider-choose-one').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.choose-one-btn--prev'),
    nextArrow: $('.choose-one-btn--next'),
})
$('.slider-choose-two').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.choose-two-btn--prev'),
    nextArrow: $('.choose-two-btn--next'),
})
$('.slider-choose-three').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.choose-three-btn--prev'),
    nextArrow: $('.choose-three-btn--next'),
})
$('.slider-choose-four').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.choose-four-btn--prev'),
    nextArrow: $('.choose-four-btn--next'),
})
$('.slider-walk').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    prevArrow: $('.walk-btn--prev'),
    nextArrow: $('.walk-btn--next'),
    dots: true,
    appendDots: $('.walk-slider__pagination'),
    dotsClass: 'walk-slider__dots',
})

$('.plan-btn').on('click', function(){
    $('.plan-btn').removeClass('floor-btn--active');
    $(this).addClass('floor-btn--active');
    console.log($('.info-house__floor-plan img'));
    $('.info-house__floor-plan img').attr('src',$(this).data('plan'));
})

$.getJSON("schema_circles.json", function (json) {

    for (var i = 0; i < json.length; i++) {


        var link, $schema_circle, marker, reserve;
        switch (json[i].finish){
            case '1':
                link = 'one';
                break;
            case '2':
                link = 'two';
                break;
            case '3':
                link = 'three';
                break;
            case '4':
                link = 'four';
                break;
        }
        $schema_circle = $('<div class="schema-point__tooltip schema-info ' +json[i].color+ '"><a class="schema-info__link link-house" data-house="' + json[i].num + '" href="'+ link + '.html"></a><div class="schema-info__number">' + json[i].num + '</div><div class="schema-info__title">КОТТЕДЖ '+ json[i].num +'</div><div class="schema-info__char"><div class="schema-info__char-name">Стоимость</div><div class="schema-info__val schema-info__val--price">' + json[i].price + '</div></div><div class="schema-info__char"><div class="schema-info__char-name">Площадь</div><div class="schema-info__val">' + json[i].square + ' м<sup>2</sup></div></div><div class="schema-info__char"><div class="schema-info__char-name">Площадь участка</div><div class="schema-info__val">' + json[i].sot + ' сот.</div></div></div>');
        marker = $('.schema__point[data-number=' + json[i].num + ']');
        // console.log($('.schema__point[data-number=' + json[i].num + ']'));
        marker.addClass(json[i].color);
        marker.find('.schema__number').addClass(json[i].color);
        marker.append($schema_circle);
        reserve = $('<div class="info-numbers__one ' + json[i].color + '"><a href="' + link + '.html" class="info-numbers__link link-house" data-house="' + json[i].num + '"></a>' + json[i].num +'</div>'),
        infoNumber = $('.info-numbers[data-reserve=' + json[i].finish + ']');
        // console.log(infoNumber);
        infoNumber.append(reserve);
    }
    housesNumber();
});
$('.schema-info__link').on('click', function(e){e.preventDefault();console.log($(this).parents('.schema__point').data('number'));})
$('.toplan').on('click', function() { $('html, body').animate({scrollTop: $('.plan').offset().top }, {duration: 370, easing: "linear"}); return false; });
$('.callback-btn').on('click', function() { $('html, body').animate({scrollTop: $('.callback').offset().top }, {duration: 370, easing: "linear"}); return false; });

$('input[type=tel').mask("8(999) 999-9999");
document.addEventListener('click', function(e) {
    var map = $('#map iframe')
    if(e.target.id === 'map') {
      map.css('pointerEvents', 'all');
    } else {
      map.css('pointerEvents', 'none');
    }
});

function housesNumber(){
    $('.link-house').on('click', function(e){
        // e.preventDefault();
        // console.log($(this).attr('href'));
        switch ($(this).attr('href')){
            case 'one.html':
                $.cookie('house-number-one', $(this).data('house'));
                console.log($.cookie('house-number-one'));
                break;
            case 'two.html':
                $.cookie('house-number-two', $(this).data('house'));
                console.log($.cookie('house-number-two'));
                break;
            case 'three.html':
                $.cookie('house-number-three', $(this).data('house'));
                console.log($.cookie('house-number-three'));
                break;
            case 'four.html':
                $.cookie('house-number-four', $(this).data('house'));
                console.log($.cookie('house-number-four'));
                break;
        }
    });
};
function houseProps(number){
    $.getJSON("schema_circles.json", function (json) {
        for (var i = 0; i < json.length; i++) {
            if (number == json[i].num){
                $('.house-price').html(json[i].price);
                $('.house-sot').html(json[i].sot);
            }
        };
    });
};


if ($.cookie('house-number-one')){
    houseProps($.cookie('house-number-one'));
    $('.house-number-one').html($.cookie('house-number-one'));
}
if ($.cookie('house-number-two')){
    houseProps($.cookie('house-number-two'));
    $('.house-number-two').html($.cookie('house-number-two'));
}
if ($.cookie('house-number-three')){
    houseProps($.cookie('house-number-three'));
    $('.house-number-three').html($.cookie('house-number-three'));
}
if ($.cookie('house-number-four')){
    houseProps($.cookie('house-number-four'));
    $('.house-number-four').html($.cookie('house-number-four'));
}

function openModal(btn){
    $('.modal-video').toggleClass('modal-video--open');
    $('.modal__video').attr('src', btn.data('video'))
}
function closeModal(){
    $('.modal-video').toggle('modal-video--open');
}

$('.walk__btn').on('click', function(){
    openModal($(this));
})
$('.modal__dark').on('click', function(){
    closeModal();
})

$('form').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        url: 'send.php',
        type: 'POST',
        contentType: false,
        processData: false,
        data: new FormData(this),
        success: function(msg) {
            setTimeout('window.location.href = "/thanks.html"', 1);
        },
        error: function(){
            alert('При отправке произошла ошибка, попробуйте позже или позвоните нам!');
        },


    });
});